# Article 5263

## Referee's Comments

### General Comments

The paper makes several valuable contributions to the algebraic theory of the fixed template Constraint Satisfaction Problems (CSPs).

The famous dichotomy conjecture of Feder and Vardi (The 2018 Alonzo Church Award) predicts that every fixed finite template CSP is either in P or NP-complete. A refinement of Bulatov, Jeavons, and Krokhin (SICOMP 2015) has predicted a precise borderline and proved the hardness part. On the algorithmic side, two basic polynomial algorithmic ideas exists and their applicability is fully understood. It was widely believed that all the tractable cases are solvable by some (complicated) combination of these two basic ideas. And indeed, the recent announcements of the solution of the dichotomy conjecture (Bulatov and Zhuk, independently, FOCS'17 best paper awards) both use such a combination.

The results presented in this paper predates the work of Bulatov and Zhuk and also combine (in a simpler way) the two algorithms. The contributions are:

- "Rectangularity Theorem". This theorem gives (under certain assumptions) a strong structural properties of relations which appear in the interesting instances of CSPs (that is, instances which were conjectured tractable by Bulatov, Jeavons, and Krokhin). This is perhaps the main mathematical contribution.
It provides a tool to the algorithmic results of the paper and it is of independent mathematical interest. As authors discuss, the result was announced in 2015 by Barto and Kozik at a conference, but a full proof has not been published (and it is possible that it contains gaps as the discussion in the paper suggests). The authors provide an independent, full proof.

- Several techniques to combine the algorithms. Among them the "Chain over Maltsev" algorithm - another result which was announced by Markovic and McKenzie and was not published.

- The authors used the collected tools to prove that each algebra with at most 4 elements and a single binary commutative operation determines a tractable CSP. This is an interesting special case of the dichotomy conjecture.

The theorems of Bulatov and Zhuk do not make this paper obsolete: the presented results may contribute to a simpler proof, the results have a potential to be applied elsewhere (in particular, in universal algebra), and the paper can serve as a source of examples (both in CSP and universal algebra).

The presentation of the results is also very good and I have only minor comments listed below.

I recommend the paper be accepted to LMCS.


### Minor Comments

- the introduction and some other places needs adjustments because of the theorem of Bulatov and Zhuk
- It could be helpful for the reader to add some comment about the condition \eta_i \neq \eta_j (used e.g. in Lemma 5.4 4th item) - that the opposite means a redundant coordinate 
- p2, Conjecture 1.1, l1: Taylor term IN its clone
- p2, line above Theorem 1.2: The shortcut [FMar] for the authors Freese, McKenzie seems strange to me
- p7, sec4.1: The definition of linked includes subdirectness, but is used for non-subdirect relations in, e.g., Lemma 4.11. I suggest to omit the subdirectness requirement. (Also, as presented, it is a bit confusing to use Proj_0 R instead of A_0 in the definition of linked and Fact 4.1.)
- p9, proof of Lemma 4.9: so the result follows FROM
- p9, 4 lines above Lemma 4.10: "If C is a subuniverse..." Add "and there is an operation that depends on more than 1 argument"
- p10, below Corollary 4.15: correct the first sentence
- p10, Cor.4.16 and elsewhere (eg. p34, l4): instead of "subdirect product of X x Y", shouldn't it rather be "subdirect product of X and Y"?
- p10, proof of Cor.4.16, 2nd part: argue that the intersection is subdirect Lemma 4.12 and mention here that 4.13 is also used. Also, the last but one line "B_0 x A_1" -> "A_0 x B_1"
- p11, proof of Lemma 4.18 (ii). Refer to Lemma 4.13 as well
- p17, item (iv): If the constraint relations are not assumed subdirect, then this does not directly corresponds to CSP(S(A)) in your notation since the projections of two constraints onto the same variables can be different (one needs to first impose arc-consistency). Please expand a bit.
- p18, Theorem 6.3 (and elsewhere): It is not defined what tractable means for an algebra, please define it (it is actually first defined in the proof of Thm 7.7 if I am not overlooking something).
- p19, item (1): It is a bit confusing when it is said that "Cor 5.8. assumes ... we can efficiently find a solution..." since Cor 5.8. does not talk about efficient computation at all. Please improve the discussion.
- p20, Prop 6.8 (and the sentence above): The final claim in the statement is incorrect - it should be that one of the intersections is empty
- p21, above Fact 6.10: Please add a refernce to a paper where "transitive" is introduced
- p26, paragraph above Ex 7.4: The discussion is a bit misleading since in the later situation it does not seem to be important that the quotient instance has few solutions (rather, a single solution is used). Please improve. It could be also helpful to say that Ex. 7.4 will handle one of the cases in Section 8
- p29, Sec 7.3, 2nd paragraph, "from a variety, E, ....": varieties were previously in a different font.
- p30, l1, "...semilattice under t, w ...": add "then" after the comma.
- p30. l8 (the displyed line): the whole "k+1" should be underlined
- p31, above Thm 8.2: Perhaps mention that it is enough to check the condition for CTB for basic operations (since later this is what's done)
- p32, Cor 8.4, 2nd paragraph: Most readers will not be familiar with the notions "strongly irregular" and "Plonka sum". Please add a reference.
- p33, case n=3, A simple, "... there are 2 algebras": add "non-isomorphic"
- p34, l4,"Therefore, by 8.4....": add "Corollary"
- p34, above the case n=4, A simple: It is not quite clear why A is EC (the definition talks about edge terms...). Please give more details.
- p35, l4 (1st stence of 2nd paragraph): the absorbing term does not work for i=2 (x=0, y=3), one more iteration of the construction is needed.
-* p35, last but one paragraph: The way how redunant coordinates are removed does not work at all (for example, equality constraints will become void after the construction - this can clearly change the solvability of the instance)
-* p36, 2nd paragraph: {0} is not a sink of A by the definition - consider the basic operation. It is just a sink w.r.t. the operation t


### Author's Responses

| page | Referee Comment | Author Response |
| --- | --- | --- |
|    | the intro & other places needs adjustments because of Bulatov and Zhuk Thm                   |  |
|    | could be helpful to add comment about the condition $\eta_i \neq \eta_j$ (e.g. in Lemma 5.4)    |  |
|    | that the opposite means a redundant coordinate                                                   |  |
| 2  | Conjecture 1.1, l1: Taylor term IN its clone                                               | fixed |
| 2  | line above Theorem 1.2: The shortcut [FMar] for the authors Freese, McKenzie seems strange to me  | changed to [FM16] |
| 7  | sec4.1: The definition of linked includes subdirectness, but is used for non-subdirect relations in, e.g., Lemma 4.11. I suggest to omit the subdirectness requirement. (Also, as presented, it is a bit confusing to use Proj_0 R instead of A_0 in the definition of linked and Fact 4.1.) | changed to subsets of products; however, we need Proj_0 R, since the elements must be first coordinates of pairs in R (not simply elements of A_0) |
| 9  | proof of Lemma 4.9: so the result follows FROM                                     | fixed |
| 9  | 4 lines above Lemma 4.10: "If C is a subuniverse..." Add "and there is an operation that depends on more than 1 argument" | omitted the offending sentence |
| 10 | below Corollary 4.15: correct the first sentence | fixed (paragraph rewritten) |
| 10 | Cor.4.16 and elsewhere (eg. p34, l4): instead of "subdirect product of X x Y", shouldn't it rather be "subdirect product of X and Y"? | There's no difference, and "subdirect product of X x Y" may be easier to read. |
| 10 | proof of Cor.4.16, 2nd part: argue that the intersection is subdirect Lemma 4.12 and mention here that 4.13 is also used. Also, the last but one line "B_0 x A_1" -> "A_0 x B_1"                                      | fixed |
| 11 | proof of Lemma 4.18 (ii). Refer to Lemma 4.13 as well                              | fixed |
| 17 | item (iv): If the constraint relations are not assumed subdirect, then this does not directly correspond to CSP(S(A)) in your notation since the projections of two constraints onto the same variables can be different (one needs to first impose arc-consistency). Please expand a bit.                       |  TO DO |
| 18  | Theorem 6.3 (and elsewhere): It is not defined what tractable means for an algebra, please define it (it is actually first defined in the proof of Thm 7.7 if I am not overlooking something).               | It was defined in the last paragraph on page 17, but has been reworded for clarity. |
| 19  | item (1): It is a bit confusing when it is said that "Cor 5.8. assumes ... we can efficiently find a solution..." since Cor 5.8. does not talk about efficient computation at all. Please improve the discussion.          | fixed |
| 20  | Prop 6.8 (and the sentence above): The final claim in the statement is incorrect---it should be that one of the intersections is empty                                                     | fixed |
| 21  | above Fact 6.10: Please add a refernce to a paper where "transitive" is introduced                       | fixed |
| 26  | paragraph above Ex 7.4: The discussion is a bit misleading since in the later situation it does not seem to be important that the quotient instance has few solutions (rather, a single solution is used). Please improve. It could be also helpful to say that Ex. 7.4 will handle one of the cases in Section 8 | TO DO |
| 29   | Sec 7.3, 2nd paragraph, "from a variety, E, ....": varieties were previously in a different font. | fixed |
| 30   | l1, "...semilattice under t, w ...": add "then" after the comma.                                  | fixed |
| 30   | l8 (the displyed line): the whole "k+1" should be underlined | fixed |
| 31   | above Thm 8.2: Perhaps mention that it is enough to check the condition for CTB for basic operations (since later this is what's done) | Not fixed. (I don't think this is what's done in our paper.) |
| 32   | Cor 8.4, 2nd paragraph: Most readers will not be familiar with the notions "strongly irregular" and "Plonka sum". Please add a reference. | fixed (added ref and definitions) |
| 33   | case n=3, A simple, "... there are 2 algebras": add "non-isomorphic" | fixed |
| 34   | l4,"Therefore, by 8.4....": add "Corollary" | fixed |
| 34   | above the case n=4, A simple: It is not quite clear why A is EC (the definition talks about edge terms...). Please give more details. | TODO |
| 35   | l4 (1st stence of 2nd paragraph): the absorbing term does not work for i=2 (x=0, y=3), one more iteration of the construction is needed. | TODO |
| 35   | last but one paragraph: The way how redundant coordinates are removed does not work at all (for example, equality constraints will become void after the construction - this can clearly change the solvability of the instance) | TODO |
| 36   | 2nd paragraph: {0} is not a sink of A by the definition - consider the basic operation. It is just a sink w.r.t. the operation t | TODO |

