
In this section we give a precise definition of what we mean by a ``constraint
satisfaction problem,'' and what it means for such a problem to be ``tractable.''
We then give some examples demonstrating how one uses the algebraic tools we
have developed to prove tractability.



\subsection{Definition of a constraint satisfaction problem}
\label{sec:defin-constr-satisf}
We now define a ``constraint satisfaction problem'' in a way that is convenient
for our purposes. This is not the most general definition possible, but for now
we postpone consideration of the scope of our setup.

Let $\bA = \<A, \sF\>$ be a finite idempotent algebra,
and let $\Sub(\bA)$ and $\sansS(\bA)$ denote the set of subuniverses and subalgebras
of $\bA$, respectively. 
\begin{definition}
  \label{def:csp}
  Let $\mathfrak{A}$ be a collection of algebras of the same similarity type.
  We define $\CSP(\mathfrak{A})$ to be the following decision problem:
\begin{itemize}
\item  An  \defn{$n$-variable instance} of $\CSP(\mathfrak{A})$
  is a quadruple $\<\sV, \sA, \sS, \sR\>$ consisting of
  \begin{itemize}
  \item a set $\sV$ of $n$ \emph{variables}; often we take $\sV$ to be
    $\nn= \{0, 1, \dots, n-1\}$;
  \item a list $\sA = (\bA_0, \bA_1, \dots, \bA_{n-1})\in \mfA^n$ of
    algebras from $\mathfrak{A}$, one for each variable;
  \item a list $\sS = (\sigma_0, \sigma_1, \dots, \sigma_{J-1})$
    of \emph{constraint scope functions}
    with arities $\ar(\sigma_j) = m_j$;
  \item a list $\sR = (R_0, R_1, \dots, R_{J-1})$ of \emph{constraint relations}, where
    each $R_j$ is the universe of a subdirect product of the algebras
    in $\sA$ with indices in $\im \sigma_j$; that is,
    \[\bR_j \sdp \prod_{0\leq i < m_j}\bA_{\sigma_j(i)}.\]
  \end{itemize}
\item A \defn{solution} to the instance $\<\sV, \sA, \sS, \sR\>$
  is an assignment %  $f \in \prod_{i\in \sV}A_i$ 
  $f \colon \sV \to \bigcup_{\nn}A_i$ %% $f: \sV \to A$
  of values to variables that satisfies all constraint relations.  More precisely,
  $f\in \myprod_{\nn}A_i$ and $f \circ \sigma_j \in R_j$ holds for all $0\leq j < J$,
  
\end{itemize}
\end{definition}
We will denote the set of solutions to the instance $\<\sV, \sA, \sS,\sR\>$ by $\Sol(\sS,\sR, \nn)$.

\begin{remark}\
  \begin{enumerate}[(i)] %[label=(\roman*)]
  \item The $i$-th scope function $\sigma_i \colon \mm_i \to \sV$ picks out the $m_i$
    variables from $\sV$ involved in the constraint relation $R_i$. Thus, the
    list $\sS$ of scope functions belongs to $\myprod_{i< J}\sV^{m_i}$.
  \item
    Frequently we require that the arities of the scope functions be bounded
    above, i.e., $\ar(\sigma_j) \leq m$, for all $j<J$. This gives rise to the
    \emph{local constraint satisfaction problem} $\CSP(\mathfrak A,m)$ consisting
    of instances of this restricted type.  
  \item
    If $(\sigma, R) \in \sC$ is a constraint of an $n$-variable instance
    %% $\<\sV, \sA, \sC\>$,
    then we denote by $\Sol((\sigma, R), \nn)$ the set of all tuples in $\prod_{\nn}A_i$
    that satisfy $(\sigma, R)$. 
    In fact, we already introduced the notation $R^{\overleftarrow{\sigma}}$ for this set
    in~(\ref{eq:19}) of Section~\ref{sec:proj-scop-kern}.  Recall, 
    $\bx \in R^{\overleftarrow{\sigma}}$ iff $\bx \circ \sigma \in R$. Thus, 
    \[
    \Sol((\sigma, R), \nn) = R^{\overleftarrow{\sigma}}
    := \bigl\{\bx \in \prod_{i\in \nn}A_i %\prod_{i\in \nn}A_i
    \mid \bx \circ \sigma \in R\bigr\}.
    \]
    Therefore, the set of solutions to the instance  $\<\sV, \sA, \sC\>$ is
    \[\Sol(\sC, \nn) = \bigcap_{(\sigma, R) \in \sC} R^{\overleftarrow{\sigma}}.\]

\item If $\mathfrak A$ contains a single algebra, we write $\CSP(\bA)$
    instead of $\CSP(\{\bA\})$.  It is important to note that, in our definition of an
    instance of $\CSP(\mathfrak A)$, a constraint relation is a subdirect product of
    algebras in $\mathfrak A$. This means that the constraint relations of an
    instance of $\CSP(\bA)$ are subdirect powers of $\bA$.
    In the literature it is conventional to allow constraint relations
    of $\CSP(\bA)$ to be subpowers of $\bA$. 
    Such interpretations would correspond to instances of
    $\CSP(\sansS (\bA))$  in our notation.
  \end{enumerate}
\end{remark}


\subsection{Instance size and tractability}
\label{sec:inst-size-tract}
We measure the computational complexity of an algorithm for solving instances
of $\CSP(\mathfrak A)$ as a function of input size. In order to do this, and to say
what it means for $\CSP(\mathfrak A)$ to be ``computationally tractable,'' we first
need a definition of input size. 
In our case, this amounts to determining the number of bits required to completely
specify an instance of the problem. In practice, an upper bound on the size is
usually sufficient.  

Using the notation in Definition~\ref{def:csp} as a guide, we bound the size of
an instance $\sI=\<\sV, \sA, \sS, \sR\>$ of $\CSP(\mathfrak A)$. Let
$q=\max(\card{A_0},\, \card{A_1},\dots,\card{A_{n-1}})$, let $r$ be the maximum
rank of an operation symbol in the similarity type, and $p$ the number of
operation symbols. Then each member of the list $\sA$ requires at most $pq^r\log
q$ bits to specify. Thus 
\begin{equation*}
\size(\sA) \leq npq^r\log q.
\end{equation*}
Similarly, each constraint scope $\sigma_j\colon \mm_j \to \nn$ can be encoded
using $m_j\log n$ bits. Taking $m=\max(m_1,\dots,m_{J-1})$ we have 
\begin{equation*}
\size(\sS) \leq Jm\log n.
\end{equation*}
Finally, the constraint relation $R_j$ requires at most $q^{m_j}\cdot m_j \cdot
\log q$ bits. Thus 
\begin{equation*}
\size(\sR) \leq Jq^m\cdot m\log q.
\end{equation*}
Combining these encodings and using the fact that $\log q \leq q$, we deduce that
\begin{equation}\label{eqn:size}
\size(\sI) \leq npq^{r+1} + Jmq^{m+1} + Jmn. 
\end{equation}
In particular, for the problem $\CSP(\mathfrak A,m)$, the parameter $m$ is
considered fixed, as is~$r$. 
In this case, we can assume $J\leq n^m$. Consequently $\size(\sI) \in O((nq)^{m+1})$ 
which yields a polynomial bound (in $nq$) for the size of the instance.

A problem is called \defn{tractable} if there exists a deterministic
polynomial-time algorithm solving all instances of that problem.
We can use Definition~\ref{def:csp} above to classify the complexity of an algebra
$\bA$, or collection of algebras $\mathfrak A$, according to the complexity of their
corresponding constraint satisfaction problems.

An algorithm $\sansA$ is called a \defn{polynomial-time algorithm}
for $\CSP(\mathfrak A)$ %(or \defn{runs in polynomial-time}
if there exist constants $c$ and $d$ such that, given an instance $\sI$ of
$\CSP(\mathfrak A)$ of size $S= \size(\sI)$,
$\sansA$ halts in at most $c S^d$ steps and outputs
whether or not $\sI$ has a solution.  In this case, we say $\sansA$
``solves the decision problem $\CSP(\mathfrak A)$ in polynomial time''
and we call the algebras in $\mathfrak A$ ``jointly tractable.''
Some authors say that an algebra $\bA$ as tractable when
$\mathfrak A = \sansS(\bA)$ is jointly tractable,
or when $\mathfrak A = \sansS \sansP_{\mathrm{fin}} (\bA)$
is jointly tractable.
We say that $\mathfrak A$ is \emph{jointly locally tractable} if, for every
natural number, $m$, there is a polynomial-time algorithm $\sansA_m$ that solves
$\CSP(\mathfrak A,m)$.  

We wish to emphasize that, as is typical in computational complexity, the
problem $\CSP(\mathfrak A)$ is a \emph{decision problem,} that is, the algorithm
is only required to respond ``yes'' or ``no'' to the question of whether a
particular instance has a solution, it does not have to actually produce a
solution. However, it is a surprising fact that if $\CSP(\mathfrak A)$ is
tractable then the corresponding \emph{search problem} is also tractable, in
other words, one can design the algorithm to find a solution in polynomial time,
if a solution exists,  
see~\cite[Cor~4.9]{MR2137072}.





\subsection{Sufficient conditions for tractability}
\label{ssec:edge-sdm}
A lattice is called \emph{meet semidistributive} if it satisfies the quasiidentity
\begin{equation*}
x\meet y \approx x\meet z \rightarrow x\meet y \approx x\meet (y\join z).
\end{equation*}
A variety is \sdm if every member algebra has a meet semidistributive
congruence lattice. Idempotent \sdm varieties are known to be
Taylor~\cite{HM:1988}. In~\cite{MR2893395}, Barto and Kozik proved the
following. 

\begin{theorem}\label{thm:sdm-tractable}
Let \bA\ be a finite idempotent algebra lying in an \sdm variety. Then \bA\ is tractable. 
\end{theorem}

A second significant technique for establishing tractability is the ``few
subpowers algorithm,'' which, according to its discoverers, is a broad
generalization of Gaussian elimination. 

\begin{definition}\label{defn:edge-term}
Let $\var{V}$ be a variety and $k$ an integer, $k>1$. A $(k+1)$-ary term $t$ is
called a \emph{$k$-edge term for $\var{V}$} if the following $k$ identities hold
in $\var{V}$: 
\begin{align*}
t(y,y,x,x,x,\dots,x) &\approx x\\
t(y,x,y,x,x,\dots,x) &\approx x\\
t(x,x,x,y,x,\dots,x) &\approx x\\
&\vdots\\
t(x,x,x,x,x,\dots,y) &\approx x.
\end{align*}
\end{definition}

Clearly every edge term is idempotent and Taylor. It is not hard to see that
every \malcev term and every near unanimity term is an edge term. Combining the
main results of \cite{MR2563736} and~\cite{MR2678065} yields the following
theorem. 

\begin{theorem}\label{thm:edge-tractable}
Let \bA\ be a finite idempotent algebra with an edge term. Then \bA\ is tractable. 
\end{theorem}

Finally, we comment that tractability is largely preserved by familiar algebraic constructions.

\begin{theorem}[\cite{MR2137072}]\label{thm:HSP-tract}
Let \bA\ be a finite, idempotent, tractable algebra. Every subalgebra and finite
power of \bA\ is tractable. Every homomorphic image of \bA\ is locally
tractable.  
\end{theorem}



\subsection{Rectangularity Theorem: obstacles and applications}
The goal of this section is to consider aspects of the Rectangularity Theorem 
that seem to limit its utility as a tool for proving tractability of \csps.
%% instance has a solution, and to prove tractability of $\CSP(\mathfrak A)$.
We first give a brief overview of the potential obstacles, and then consider
each one in more detail in the following subsections.
\begin{enumerate}
\item
  \label{item:abelian-potatoes-tractable}
  {\bf Abelian factors must have easy partial solutions.}
  One potential limitation concerns the abelian factors in the product 
  algebra associated with a \csp instance.
  Indeed, %% Corollaries \ref{cor:RT-cor} and
  Corollary~\ref{cor:RT-cor-gen} assumes that
  when the given constraint relations are projected onto abelian factors,
  we can efficiently find a solution to this restricted instance---that is,
  an element satisfying all constraint relations after projecting these
  relations onto the abelian factors of the product.  
  Section~\ref{sec:tract-abel-algebr} shows that this concern is easily
  dispensed with and is not a real limitation of
  Corollary~\ref{cor:RT-cor-gen}.

\item {\bf Intersecting products of minimal absorbing subalgebras.}
  Another potential obstacle concerns the nonabelian simple factors.
  As we saw in Section~\ref{sec:rect-theor}, the Rectangularity Theorem
  (and its corollaries) assumes that the universes of the subdirect
  products in question all intersect nontrivially
  with a single product $\myprod B_i$ of minimal absorbing subuniverses.
  (We refer to ``minimal absorbing subuniverses'' quite frequently, so from now
  on we call them \defn{masses}; that is, a \defn{mass} is 
  a {\bf m}inimal {\bf a}bsorbing {\bf s}ubuniver{\bf s}e, and the product of
  \masses will be called a \defn{mass product}.)
  Moreover, assumption~(\ref{item:RT-cor-gen-4}) of Corollary~\ref{cor:RT-cor-gen}
  requires that all constraint relations intersect nontrivially with a single
  \mas product. This is a real
  limitation, as we demonstrate in Section~\ref{sec:mass-products} below.

\item {\bf Nonabelian factors must be simple.}
  This is the most obvious limitation of the theorem and at this point
  we don't have a completely general means of overcoming it.  However,
  some methods that work in particular cases are described below.

\end{enumerate}

In the next two subsections we address potential limitations (1) and (2).
In Section~\ref{sec:var-reduc} we develop some alternative methods for
proving tractability of nonsimple algebras, and then
in Section~\ref{sec:csps-comm-idemp} we apply these methods 
in the special setting of ``commutative idempotent binars.''

\subsubsection{Tractability of abelian algebras}
\label{sec:tract-abel-algebr}
To address concern~(\ref{item:abelian-potatoes-tractable})
of the previous subsection, we observe that finite abelian algebras yield
tractable \csps.
We will show how to use this fact and the Rectangularity Theorem to find 
solutions to a given \csp instance (or determine that none exists).
To begin, recall the fundamental result of tame congruence
theory~\cite[Thm~7.12]{HM:1988}  that we 
reformulated as Lemma~\ref{lem:HM-thm-7-12}.
As noted in Remark~\ref{rem:abelian-quotients} above,
this result has the following important corollary.
(A proof that avoids tame congruence theory appears in~\cite[Thm~5.1]{MR3374664}.)
\begin{theorem}
  \label{thm:type2cp}
Let $\var{V}$ be a locally finite variety with a Taylor term. Every finite abelian member of $\var{V}$ generates a congruence-permutable variety. Consequently, every finite abelian member of $\var{V}$ is tractable.
\end{theorem}

Let $\bA$ be a finite algebra in a Taylor variety 
and fix an instance $\sI = \<\sV, \sA, \sS, \sR\>$ of $\CSP(\sansS(\bA))$
with $n = |\sV|$ variables.
Suppose all nonabelian algebras in the list $\sA$ are simple.
Let $\alpha \subseteq \nn$ denote the indices of the abelian algebras in $\sA$,
and assume without loss of generality that $\alpha = \{0,1,\dots, q-1\}$.
That is, $\bA_0$, $\bA_1$, $\dots$, $\bA_{q-1}$ are finite idempotent abelian algebras. Consider
now the restricted instance $\sI_\alpha$ obtained by dropping all constraint relations
with scopes that don't intersect $\alpha$, and by restricting the remaining constraint
relations to the abelian factors.
Since the only algebras involved in $\sI_\alpha$ are abelian, this is an instance of a
tractable \csp.  Therefore, we can check in polynomial-time whether or not $\sI_\alpha$
has a solution.
If there is no solution, then the original instance $\sI$ has no solution.
On the other hand, suppose
$f_\alpha\in \myprod_{j\in \alpha}A_j$ is a solution to $\sI_\alpha$.
In Corollary~\ref{cor:RT-cor-gen},
to reach the conclusion that the full instance has a solution, we 
required a partial solution $\bx \in \bigcap \Proj_\alpha R_\ell$.
This is precisely what $f_\alpha$ provides.

To summarize, we try to find a partial 
solution by restricting the instance to abelian factors and, if such a partial solution exists,
we use it for $\bx$ in Corollary~\ref{cor:RT-cor-gen}.
Then, assuming the remaining hypotheses of Corollary~\ref{cor:RT-cor-gen} hold,
we conclude that a solution to the original instance exists.
If no solution to the restricted instance exists, then the original instance has no solution.
Thus we see that assumption~(\ref{item:RT-cor-gen-5}) of
Corollary~\ref{cor:RT-cor-gen} does not limit the application scope of this result.

\subsubsection{Mass products}
\label{sec:mass-products}
This section concerns products of minimal absorbing subalgebras, or
``\mas products.'' Hypothesis~(\ref{item:RT-cor-gen-4}) of
Corollary~\ref{cor:RT-cor-gen} assumes that all constraint relations intersect
nontrivially with a single \mas product.
However, it's easy to contrive instances where this hypothesis does not hold.
For example, take the algebra $\bA = \<\{0,1\}, m\>$,
where $m \colon A^3 \to A$ is the ternary idempotent majority operation---that is,
$m(x,x,x)\approx x$ and $m(x,x,y)\approx m(x,y,x)\approx m(y,x,x) \approx x$.
Consider subdirect products $\bR = \<R, m\>$ and $\bS=\<S, m\>$ of
$\bA^3$ with universes
  \begin{align*}
  R &= \{(0,0,0), (0,0,1), (0,1,0), (1,0,0)\},\\
  S &= \{(0,1,1), (1,0,1), (1,1,0), (1,1,1)\}.
  \end{align*}
  Then there are \mas products that intersect nontrivially with either $R$ or $S$, 
  but no \mas product intersects nontrivially with both $R$ and $S$. 
  As the Rectangularity Theorem demands,
  each of $R$ and $S$ fully contains every \mas product that it intersects, but there is no
  single \mas product intersecting nontrivially with both $R$ and $S$. Hence,
  hypothesis~(\ref{item:RT-cor-gen-4}) of Corollary~\ref{cor:RT-cor-gen} is not
  satisfied so it would seem that we cannot use rectangularity 
  to determine whether a solution exists in such instances.
  

  The example described in the last paragraph is very special.
  For one thing, there is no solution to the instance with
  constraint relations $R$ and $S$.
  We might hope that when an instance \emph{does} have a solution,
  then there should be a solution that passes through a \mas product.
  As we now demonstrate, this is not always the case.
  In fact, Example~\ref{ex:mass-products-3} describes a case in which
  two subdirect powers intersect nontrivially,
  yet each intersects trivially with every \mas product.
\begin{proposition}
  \label{claim:mass-products-2}
There exists an algebra $\bA$ with subdirect powers $\bR$ and $\bS$ 
such that 
$R \cap S \neq \emptyset$ and, for every collection $\{B_i\}$ of \masses,
$R \cap \myprod B_i = \emptyset = S \cap \myprod B_i$.
\end{proposition}

\noindent We prove Proposition~\ref{claim:mass-products-2} by simply producing 
an example that meets the stated conditions.

\begin{example}
  \label{ex:mass-products-3}
Let $\bA = \<\{0,1,2\}, \circ\>$ be an algebra with binary operation $\circ$
given by
\vskip3mm
 \begin{center}
 \begin{tabular}{c|ccc}
      $\circ$ & 0 & 1 & 2 \\
      \hline
      0 & 0 & 1 & 2\\
      1 & 1 & 1 & 0\\
      2 & 2 & 0 & 2
    \end{tabular}
 \end{center}
\vskip3mm
The proper nonempty subuniverses of
$\bA$ are $\{0\}$, $\{1\}$, $\{2\}$, $\{0,1\}$, and $\{0,2\}$.
Note that %$B_1 = \{1\}$ and $B_2=\{2\}$
$\{1\}$ and $\{2\}$ are both minimal absorbing subuniverses with respect to the
term $t(x,y,z,w) = (x \circ y) \circ (z \circ w)$. 
Note also that $\{0\}$ is not an absorbing subuniverse of~$\bA$. For if
$\{0\}\absorbing \bA$, then
$\{0\}\absorbing \{0,1\}$ which is  false, since $\{0,1\}$ is a semilattice with
absorbing element $1$.  

Let $\bA_0 \cong \bA \cong \bA_1$, $R = \{(0,0), (1,1), (2,2)\}$, and
$S = \{(0,0), (1,2), (2,1)\}$.  Then $\bR$ and $\bS$ are subdirect products of
$\bA_0\times \bA_1$ and $R\cap S= \{(0,0)\}$.  There are four minimal absorbing subuniverses of
$\bA_0 \times \bA_1$.  They are
\[
B_{11} = \{1\}\times \{1\} = \{(1,1)\} , \; B_{12} = \{(1, 2)\}, \; B_{21} = \{(2, 1)\},
\; B_{22} = \{(2, 2)\}.
\]
Finally, observe
\[
R\cap B_{ij} = \begin{cases}
  \{(i,j)\}, & i=j,\\
  \emptyset, & i\neq j.
\end{cases}
\qquad 
S\cap B_{ij} = \begin{cases}
  \emptyset, & i=j\\
  \{(i,j)\}, & i\neq j.
\end{cases}
\]
This proves Proposition~\ref{claim:mass-products-2}.

\end{example}



\subsection{Algorithm synthesis for heterogeneous problems}
\label{sec:heter-potat}
We now present a new result (Theorem~\ref{thm:fry-pan2})
that, like the Rectangularity Theorem, aims to describe some of the elements that
must belong to certain subdirect products.
The conclusions we draw are weaker than those of
Theorem~\ref{thm:rectangularity}.
However, the hypotheses required here are simpler, and the motivation
is different.

We have in mind subdirect products of ``heterogeneous'' 
families of algebras. What we mean by this is 
described and motivated as follows:
let $\sC_1$, $\dots$, $\sC_m$
be classes of algebras, all of the same signature. Perhaps we are lucky enough
to possess a single algorithm that
proves the algebras in $\bigcup\sC_i$ are jointly
tractable (defined in Section~\ref{sec:inst-size-tract}).
Suppose instead that we are a little less fortunate
and, although we have no such single
algorithm at hand, at least we do happen to know that
the classes $\sC_i$ are \emph{separately tractable}.
By this we mean that for each $i$ we have a 
proof (or algorithm) $\P_i$ witnessing the tractability of
$\CSP(\sC_i)$.
It is natural to ask whether and under what conditions it might be possible to
derive from $\{\P_i \mid 1\leq i \leq m\}$ a single proof (algorithm)
establishing that the algebras in 
$\bigcup \sC_i$ are \emph{jointly tractable},
so that $\CSP(\bigcup \sC_i)$ is tractable.

The results in this section take a small
step in this direction by considering two special
classes of algebras that are known to be separately tractable,
and demonstrating that they are, in fact, jointly tractable.
We apply this tool in Section~\ref{sec:block-inst} where
we prove tractability of a \csp involving algebras that were
already known to be tractable, but not previously known to be jointly tractable.


The results here involve special term operations called
cube terms and transitive terms.
A $k$-ary idempotent term $t$ is a \emph{cube term}
if for every
coordinate $i \leq k$, $t$ satisfies an identity of the form
$t(x_1, \dots, x_k) \approx y$, where
$x_1,\dots, x_k \in \{x, y\}$ and $x_i = x$.
%% (See \cite{MR2563736} and \cite{MR2900858}.)
A $k$-ary operation $f$ on a set $A$ is called
\emph{transitive in the $i$-th coordinate} if for every $u, v \in A$,
there exist $a_1,\dots, a_k \in A$ such that $a_i = u$
and $f(a_1 ,\dots, a_n) = v$. Operations
that are transitive in every coordinate are called \emph{transitive}. 


\begin{fact}
  \label{fact:cubes-are-trans}
  Let $\bA$ be a finite idempotent algebra and suppose $t$ is a cube term operation on $\bA$.
  Then $t$ is a transitive term operation on $\bA$.
\end{fact}
\begin{proof}
  Assume $t$ is a $k$-ary cube term operation on $\bA$ and fix $0\leq i<k$.  We will prove that $t$
  is transitive in its $i$-th argument.  Fix $u, v \in A$.
  We want to show there exist $a_0,\dots, a_{k-1} \in A$ such that $a_{i-1} = u$
  and $t(a_0 ,\dots, a_{k-1}) = v$. Since $t$ is a cube term, it satisfies an identity of the form
    $t(x_1, \dots, x_k) \approx y$,
  where $(\forall j)(x_j \in \{x, y\})$ and $x_i = x$.
  So we simply substitute $u$ for $x$ and $v$ for $y$ in 
  the argument list in of this identity. %Equation~(\ref{eq:2}).
  Denoting this substitution by $t[u/x, v/y]$, we have
  $t[u/x, v/y]= v$, proving that $t$ is transitive.
\end{proof}

\begin{fact}
  \label{fact:pseudovar}
  The class
  \begin{equation}
    \label{eq:0001}
    \sT = \{\bA \mid \bA \text{ finite and every subalgebra of $\bA$ has a transitive term op}\}
  \end{equation}
  is closed under the taking of homomorphic images, % ($\sansH$)
  subalgebras,  % ($\sansS$), 
  and finite products. % ($\sansP_{\mathrm{fin}}$).
  That is, $\sT$ is a \emph{pseudovariety}.
\end{fact}


We also require the following obvious fact about
nontrivial algebras in a Taylor variety. (We call an algebra \emph{nontrivial}
if it has more than one element.)
\begin{fact}
  \label{fact:nonconstant-terms-exist}
  If $\bA$ and $\bB$ are nontrivial algebras in a Taylor variety $\var{V}$, then for
  some $k>1$ there is a $k$-ary term $t$ in $\var{V}$ such that $t^{\bA}$ and
  $t^{\bB}$ each depends on at least two of its arguments.
\end{fact}

Finally, we are ready for the main result of this section.

\begin{theorem}
\label{thm:fry-pan2}
  Let $\bA_0, \bA_1, \dots, \bA_{n-1}$ be finite idempotent algebras in a Taylor
  variety and assume there exists a proper nonempty subset $\alpha \subset \nn$ such that
  \begin{itemize}
  \item $\bA_i$ has a cube term for all $i \in \alpha$,
  \item $\bA_j$ has a sink $s_j \in A_j$ for all $j \in \alpha'$;
    let $\bs \in \prod_{\alpha'}A_j$ be a tuple of sinks.
  \end{itemize}
   If $\bR \sdp \prod_{\nn} \bA_i$, then the set $X:=R_\alpha \times \{\bs\}$
  is a subuniverse of $\bR$.
\end{theorem}
\begin{remark}
To foreshadow applications of
Theorem~\ref{thm:fry-pan2}, imagine we have 
algebras of the type described, and a collection
$\sR$ of subdirect products of these algebras. Suppose also that we have somehow
determined that the intersection of the $\alpha$-projections of these subdirect
products is nonempty, say,
\[\bx_\alpha \in \bigcap_{R \in \sR} R_\alpha.\]
Then the full intersection $\bigcap \sR$ will also be nonempty, since
according to the theorem it must
contain the tuple that is $\bx_\alpha$ on $\alpha$ and $\bs$ off $\alpha$.
\end{remark}

\begin{proof}
  Fix $\bx \in X$, so $\bx\circ \alpha' = \bs$ and $\bx \circ \alpha \in R_\alpha$. 
  We will prove that $\bx \in R$.
  Define $\bA_{\alpha} =\prod_{\alpha}\bA_i$ and $\bA_{\alpha'} =\prod_{\alpha'}\bA_i$, so 
  $R_{\alpha}$ is a subuniverse of $\bA_\alpha$ and $R$ is a subuniverse of
  $\bA_\alpha \times \bA_{\alpha'}$.

  By Fact~\ref{fact:cubes-are-trans}, 
  for each $i \in \alpha$ every subalgebra of $\bA_i$
  has a transitive term operation.  Moreover, the class $\sT$ defined in~(\ref{eq:0001}) 
  is a pseudovariety, so every subalgebra of 
  $\bA_{\alpha}$ has a transitive term operation.  Suppose there are $J$ subalgebras 
  of $\bA_{\alpha}$ and let $\{t_j\mid 0\leq j < J\}$ denote the collection
  of transitive term operations, one for each subalgebra. % of $\bA_\alpha$.
  Then it is not hard to prove that 
  $t := t_0 \star  t_1 \star \cdots \star  t_{J-1}$
  is a transitive term for every subalgebra of $\bA_\alpha$.
  (Recall, $\star$ was defined at the end of Section~\ref{sec:absorption-thm}.)  
  In particular $t$ is transitive for the subalgebra with universe $R_{\alpha}$. 
  %% Fix $N$ and a
  Assume $t$ is $N$-ary.
  
  Fix $j\in \alpha'$.  As usual, $t^{\bA_j}$ denotes the interpretation of 
  $t$ in $\bA_j$. We may assume without loss of generality that $t^{\bA_j}$ 
  depends on its first argument, at position 0. (It must depend on at least one of
  its arguments by idempotence.) 
  Now, since $\bR$ is a subdirect product of $\prod_{\nn}\bA_i$, 
  there exists $\br^{(j)} \in R$ such that $\br^{(j)}(j) = s_j$, the sink in $\bA_j$.  
  Since   $\bx\circ \alpha\in R_\alpha$ and since $t^{\bA_\alpha}$
  is transitive over $\bR_\alpha$, there exist
  $\br_1, \dots, \br_{N-1}$ in $R$ such that 
  \begin{align*}
   \vy^{(j)}&:= t^{\bA_{\alpha} \times \bA_{\alpha'}}(\br^{(j)}, \br_1, \dots, \br_{N-1})\\
      &= 
    (t^{\bA_{\alpha}}(\br^{(j)}\circ \alpha, \br_1\circ \alpha, \dots, \br_{N-1}\circ \alpha),
    t^{\bA_{\alpha'}}(\br^{(j)}\circ \alpha', \br_1\circ \alpha', \dots, \br_{N-1}\circ \alpha'))
  \end{align*}
  belongs to $R$  and satisfies
  $\vy^{(j)}\circ \alpha = \bx \circ \alpha$ and
  $\vy^{(j)}(j) = s_j$.

  (To make the role played by transitivity here
  more transparent, note that we asserted the existence of elements in $R$
  whose ``$\alpha$-segments,'' $\br_1\circ \alpha, \dots, \br_{N-1}\circ \alpha$
  could be plugged in for all but one of the arguments of $t^{\bA_\alpha}$, 
  resulting in a map (unary polynomial) taking $\br^{(j)}\circ \alpha$ to
  $\bx\circ \alpha$. It is the transitivity of $t^{\bA_\alpha}$ over $\bR_\alpha$
  that justifies this assertion.)

  If $\alpha' = \{j\}$, we are done, since $\vy^{(j)} = \bx$ in that case.
  If $|\alpha'|>1$, then we repeat the foregoing procedure for each 
  $j \in \alpha'$ and obtain a subset $\{\vy^{(j)} \mid j \in \alpha'\}$ of $R$,
  each member of which agrees with $\bx$ on $\alpha$ and has a sink in some position
  $j\in \alpha'$. 

  Next, choose distinct $j, k \in \alpha'$. Suppose $w$ is a Taylor term for $\var{V}$.
  Then by Fact~\ref{fact:nonconstant-terms-exist} we may assume without loss of
  generality that $w^{\bA_j}$ depends on its $p$-th argument and $w^{\bA_k}$ depends
  on its $q$-th argument, for some $p\neq q$. Consider
  \begin{align*}
    \bz:= w^{\Pi \bA_i}(\vy^{(j)}, %% \vy^{(j)}, 
    \dots, \vy^{(j)}, & \, \vy^{(k)}, \vy^{(j)}, \dots, \vy^{(j)})\\
    %&\uparrow\\
    &\; ^{\widehat{\lfloor}} \, \text{$q$-th argument}
  \end{align*}
  Evidently, $\bz(j) = s_j$, $\bz(k) = s_k$, and 
  $\bz \circ \alpha = \bx \circ \alpha$ by idempotence, since, when restricted to
  indices in $\alpha$, all the input arguments agree and are equal to $\bx \circ \alpha$. 
  If $\alpha' = \{j, k\}$, we are done.  Otherwise, choose
  $\ell \in \alpha'-\{j, k\}$, and again $w^{\bA_\ell}$ depends on at
  least one of its 
  \ifthenelse{\boolean{footnotes}}{%
    arguments,\footnote{It
      clearly doesn't matter on which argument $w^{\bA_\ell}$ depends.}
  }{arguments,}
  say, the $u$-th.  Let
  \begin{align*}
    \bz':= w^{\Pi \bA_i}(\bz, %% \vy^{(j)}, 
    \dots, \bz, & \, \vy^{(\ell)}, \bz \dots, \bz).\\
    %&\uparrow\\
    &\; ^{\widehat{\lfloor}} \, \text{$u$-th argument}
  \end{align*}
  Then $\bz'$ belongs to $R$, agrees with $\bx$ on $\alpha$, and satisfies
  $\bz'(j) = s_j$, $\bz'(k) = s_k$, and   $\bz'(\ell) = s_\ell$. 
  Continuing in this way until the set $\alpha'$ is exhausted produces
  an element in $R$ that agrees with $\bx$ everywhere. % $\alpha \cup \alpha'$.
  In other words,  $\bx$ itself belongs to $R$.
\end{proof}

In Section~\ref{sec:block-inst} we apply
Theorem~\ref{thm:fry-pan2} 
in the special case where
``has a cube term''  in the first hypothesis 
is replaced with ``is abelian.'' Let us be explicit.

\begin{corollary}
\label{cor:fry-pan}
  Let $\bA_1, \dots, \bA_{n-1}$ be finite idempotent algebras in a
  locally finite Taylor variety. 
  Suppose there exists $0< k < n-1$ such that 
  \begin{itemize}
  \item $\bA_i$ is abelian for all $i < k$;
  \item $\bA_i$ has a sink $s_i \in A_i$ for all $i \geq k$.
  \end{itemize}
  If $\bR \sdp \prod \bA_i$, then
  $R_{\kk} \times \{s_k\} \times \{s_{k+1}\} \times \cdots \times \{s_{n-1}\}  \subseteq R$.
\end{corollary}
\begin{proof}
  Since $\bA_{\kk} := \prod_{i<k} \bA_i$ is abelian and lives in a locally finite
  Taylor variety, there exists a term $m$ such that $m^{\bA_{\kk}}$ is a \malcev
  operation on $\bA_{\kk}$ (Theorem~\ref{thm:type2cp}).
  Since a \malcev term is a cube term, the result follows from Theorem~\ref{thm:fry-pan2}.
\end{proof}

\ifthenelse{\boolean{arxiv}}{%
  As an alternative, a direct proof of Corollary~\ref{cor:fry-pan} appears in 
  Section~\ref{sec:proof-fry-pan-cor} below.
}{As an alternative, a direct proof of Corollary~\ref{cor:fry-pan} appears in
  the extended version of the present paper,~\cite{Bergman-DeMeo}.}

